terraform {
  required_providers {
    cloudavenue = {
      source = "orange-cloudavenue/cloudavenue"
      version = "0.18.2"
    }
  }
}


provider "cloudavenue" {  
  org      = var.org
  user     = var.user
  password = var.password
  #url      = var.url
  vdc      = var.vdc
}


#resource "cloudavenue_vapp" "example" {
 # name        = "example-vapp2"
  #vdc         = "Edge-Computing"
  #description = "This is an example vApp"
#}

/*
data "cloudavenue_catalog_vapp_template" "example" {
  catalog_name  = "Edge"
  template_name = "T-OBS-SLES-15-IMAGE-v1"
}

resource "cloudavenue_vapp" "example" {
  name        = "example-vapp3"
  vdc         = "Edge-Computing"
  description = "This is an example vApp"
}

resource "cloudavenue_vm" "example" {
  name        = "example-vm1"
  vdc         = "Edge-Computing"
  description = "This is a example vm"
  vapp_name = cloudavenue_vapp.example.name
  deploy_os = {
    vapp_template_id = data.cloudavenue_catalog_vapp_template.example.id
    accept_all_eulas = true
  }
  state = {
    power_on = false
  }
  resource = {
    cpus   = 4
    memory = 4096
  }
}  
*/

/*
data "cloudavenue_tier0_vrfs" "example" {}

output "vrfs" {
  value = data.cloudavenue_tier0_vrfs.example
}
*/

data "cloudavenue_tier0_vrfs" "example" {}

data "cloudavenue_edgegateway" "example" {
  name = "tn01i01ocb0006974spt101"
}

data "cloudavenue_network_routed" "example" {
  name            = "subnet-customer-edge"
  edge_gateway_id =  data.cloudavenue_edgegateway.example.id
}


data "cloudavenue_catalog_vapp_template" "example" {
  catalog_name  = "Edge"
  template_name = "T-OBS-SLES-15-IMAGE-v1"
}

resource "cloudavenue_vapp" "example" {
  name        = "example-vapp4"
  vdc         = "Edge-Computing"
  description = "This is an example vApp"
}

resource "cloudavenue_vapp_org_network" "example" {
  vapp_name    = cloudavenue_vapp.example.name
  vdc         = "Edge-Computing"
  network_name = data.cloudavenue_network_routed.example.name
}

/*
resource "cloudavenue_vm" "example" {
  name      = "k8s-master"
  vdc         = "Edge-Computing"
  vapp_name = cloudavenue_vapp.example.name
  deploy_os = {
    vapp_template_id = data.cloudavenue_catalog_vapp_template.example.id
  }
  settings = {
    guest_properties = {
      "guestinfo.hostname" = "k8s-master"
    }
    customization = {
      admin_password = "tttt"
      allow_local_admin_password = true
    }
  }
  resource = {
    cpus   = 2
    memory = 2048
    networks = [
      {
        type               = "org"
        name               = cloudavenue_vapp_org_network.example.network_name
        ip                 = "192.168.0.202"
        ip_allocation_mode = "MANUAL"
        is_primary         = true
      }
    ]
  }
}
*/

resource "cloudavenue_vm" "rancher_vm1" {
  name      = "rancher-vm1"
  vdc         = "Edge-Computing"
  vapp_name = cloudavenue_vapp.example.name
  deploy_os = {
    vapp_template_id = data.cloudavenue_catalog_vapp_template.example.id
  }
  settings = {
    guest_properties = {
      "guestinfo.hostname" = "rancher-vm1"
    }
    customization = {
      admin_password = "tttt"
      allow_local_admin_password = true
    }
  }
  resource = {
    cpus   = 2
    memory = 4096
    networks = [
      {
        type               = "org"
        name               = cloudavenue_vapp_org_network.example.network_name
        ip                 = "192.168.0.205"
        ip_allocation_mode = "MANUAL"
        is_primary         = true
      }
    ]
  }
}

resource "cloudavenue_vm" "rancher_vm2" {
  name      = "rancher-vm2"
  vdc         = "Edge-Computing"
  vapp_name = cloudavenue_vapp.example.name
  deploy_os = {
    vapp_template_id = data.cloudavenue_catalog_vapp_template.example.id
  }
  settings = {
    guest_properties = {
      "guestinfo.hostname" = "rancher-vm2"
    }
    customization = {
      admin_password = "tttt"
      allow_local_admin_password = true
    }
  }
  resource = {
    cpus   = 2
    memory = 4096
    networks = [
      {
        type               = "org"
        name               = cloudavenue_vapp_org_network.example.network_name
        ip                 = "192.168.0.206"
        ip_allocation_mode = "MANUAL"
        is_primary         = true
      }
    ]
  }
}
